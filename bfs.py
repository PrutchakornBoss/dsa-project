from BinaryTree import BinaryTree


def bfs(tree):
        current = tree.root
        queue = []
        result = []
        while current is not None:
            result.append(current.data)
            if current.left is not None:
                queue.append(current.left)
            if current.right is not None:
                queue.append(current.right)
            if len(queue) != 0:
                current = queue.pop(0)
            else:
                current = None
        return result


if __name__ == '__main__':
    tree = BinaryTree()

    data = [int(x) for x in input().split(",")]
    # data = [50, 20, 30, 40, 50]
    for i in data:
        tree.insert(i)

    bfs = bfs(tree)
    print(bfs)

    write = open("result_input.dat", "w")
    write.write(str(bfs))

