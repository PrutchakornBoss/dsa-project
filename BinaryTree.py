class BinaryTree:
    class Node:
        __slots__ = 'left', 'right', 'data'

        def __init__(self, left, right, data):
            self.left = left
            self.right = right
            self.data = data

    def __init__(self):
        self.root = None

    def isEmpty(self):
        return not self.root

    def insert(self, data):
        newNode = self.Node(None, None, data)
        if self.isEmpty():
            self.root = newNode
        else:
            parent = self.root
            while parent:
                if data >= parent.data:
                    if parent.right is None:
                        parent.right = newNode
                        break
                    parent = parent.right
                else:
                    if parent.left is None:
                        parent.left = newNode
                        break
                    parent = parent.left

    def delete(self, key):
        if self.isEmpty():
            return None
        else:
            isRight = False
            previous = None
            current = self.root
            while current.data != key:
                previous = current
                if key > current.data:
                    isRight = True
                    current = current.right
                else:
                    current = current.left
                    isRight = False

            if current.left is None and current.right is None:
                if isRight:
                    previous.right = None
                else:
                    previous.left = None
            elif current.left is None and current.right is not None:
                if previous is None:
                    self.root = current.right
                elif isRight:
                    previous.right = current.right
                else:
                    previous.left = current.right
            elif current.left is not None and current.right is None:
                if previous is None:
                    self.root = current.left
                elif isRight:
                    previous.right = current.left
                else:
                    previous.left = current.left
            else:
                previousParent = None
                parent = current.left
                while parent.right is not None:
                    previousParent = parent
                    parent = parent.right
                if parent.left is not None:
                    current.data = parent.data
                    previousParent.right = parent.left
                else:
                    current.data = parent.data
                    previousParent.right = None

    def inorder(self, root):
        if root:
            self.inorder(root.left)
            print(root.data, end=" ")
            self.inorder(root.right)


