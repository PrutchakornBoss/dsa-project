from BinaryTree import BinaryTree

def dfs(tree):
        current = tree.root
        stack = []
        result = []
        while current is not None:
            result.append(current.data)
            if current.right is not None:
                stack.append(current.right)
            if current.left is not None:
                stack.append(current.left)
            if len(stack) != 0:
                current = stack.pop()
            else:
                current = None
        return result


if __name__ == '__main__':
    tree = BinaryTree()

    data = [int(x) for x in input().split(",")]
    # data = [50, 20, 30, 40, 50]
    for i in data:
        tree.insert(i)

    dfs = dfs(tree)
    print(dfs)

    write = open("result_input.dat", "w")
    write.write(str(dfs))

